﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NepaliToUnicodeConverter
{
    class bku
    {
        List<KeyValuePair<string, string>> rep = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> List3Digit = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> List2Digit = new List<KeyValuePair<string, string>>();

        List<KeyValuePair<string, string>> ListFirstPass = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ListSecondPass = new List<KeyValuePair<string, string>>();

        List<KeyValuePair<string, string>> ListFinalPass = new List<KeyValuePair<string, string>>();

        private void init()
        {

            rep.Add(new KeyValuePair<string, string>("f", "ा"));
            //ListFinalPass.Add(new KeyValuePair<string, string>("ाे", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("्ा", "ः"));
            ListFinalPass.Add(new KeyValuePair<string, string>("्ो", "े"));
            ListFinalPass.Add(new KeyValuePair<string, string>("्ौ", "ै"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अो", "ओ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अा", "आ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("आै", "औ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("आे", "ओ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाो", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाॅ", "ॉ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाे", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंु", "ुं"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ेे", "े"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अै", "अ‍ै"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाे", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अे", "अ‍े"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंा", "ां"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अॅ", "अ‍ॅ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाै", "ौ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ैा", "ौ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंृ", "ृं"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ँा", "ाँ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ँू", "ूँ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ेा", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंे", "ें"));


            // ","",",","","","","",",",",",","",","",","",",",",",",","

            rep.Add(new KeyValuePair<string, string>("|", "्र"));
            //rep.Add(new KeyValuePair<string, string>("|", "्"));
            rep.Add(new KeyValuePair<string, string>("l", "ि"));
            rep.Add(new KeyValuePair<string, string>("{", "ॉ"));
            rep.Add(new KeyValuePair<string, string>("\"", "ू"));
            rep.Add(new KeyValuePair<string, string>("'", "ु"));
            rep.Add(new KeyValuePair<string, string>("_", "ं"));
            rep.Add(new KeyValuePair<string, string>("[", "ृ"));
            rep.Add(new KeyValuePair<string, string>(@"\", "्"));
            rep.Add(new KeyValuePair<string, string>("]", "े"));
            rep.Add(new KeyValuePair<string, string>("}", "ै"));
            rep.Add(new KeyValuePair<string, string>("Ÿ", "ः"));
            rep.Add(new KeyValuePair<string, string>("f{", "ॉ")); //शर्मा  शमॉ
            rep.Add(new KeyValuePair<string, string>("f}", "ौ"));
            rep.Add(new KeyValuePair<string, string>("f]", "ो"));
            rep.Add(new KeyValuePair<string, string>("F", "ँ"));
            rep.Add(new KeyValuePair<string, string>("L", "ी"));
            rep.Add(new KeyValuePair<string, string>("l", "ि"));

            ListSecondPass.Add(new KeyValuePair<string, string>("O{", "ई"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"o\", "य्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"<\", "र्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"km\", "फ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"Tt\", "त्त्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"em\", "झ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"s\", "क्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"v\", "ख्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"u\", "ग्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"#\", "घ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"r\", "च्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"h\", "ज्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"em", "झ"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"`\", "ञ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"t\", "त्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"y\", "थ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"w\", "ध्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"g\", "न्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"k\", "प्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"a\", "ब्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"e\", "भ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"d\", "म्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"n\", "ल्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"j\", "व्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"z\", "श्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@";\", "स्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"x\", "ह्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"!\", "ज्ञ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"q\", "त्र्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@">\", "श्र्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("ˆ", "फ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(")", "ण्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("A", "ब्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("B", "द्य"));
            ListSecondPass.Add(new KeyValuePair<string, string>("D", "म्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("E", "भ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("G", "न्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("H", "ज्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("I", "क्ष्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("J", "व्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("K", "प्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("N", "ल्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("R", "च्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("S", "क्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("T", "त्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("U", "ग्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("V", "ख्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("W", "ध्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("X", "ह्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Y", "थ्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Z", "श्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"*\(", "ड्ढ"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"If\", "क्ष्"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"b\#", "द्घ"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"b\o", "द्य"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"*\*", "ड्ड"));
            ListSecondPass.Add(new KeyValuePair<string, string>(@"if\", "ष्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Qm", "क्त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("qm", "क्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("St", "क्त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("s|", "क्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("b|", "द्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("^«", "ट्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("&«", "ठ्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("*«", "ड्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("(«", "ढ्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("$", "द्द"));
            ListSecondPass.Add(new KeyValuePair<string, string>("¢", "द्ध"));
            ListSecondPass.Add(new KeyValuePair<string, string>("‹", "ङ्घ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("§", "ट्ट"));
            ListSecondPass.Add(new KeyValuePair<string, string>("¶", "ठ्ठ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ë", "ङ्ग"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Í", "ङ्क"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ý", "ट्ठ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("å", "द्व"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));


            //List3Digit.Add(new KeyValuePair<string, string>(@"cfF", "ऑ"));
            //List3Digit.Add(new KeyValuePair<string, string>(@"cf}", "औ"));
            ////rep.Add(new KeyValuePair<string, string>(@"cf]", "ओ"));
            //List3Digit.Add(new KeyValuePair<string, string>("sL{", "र्की"));
            //List3Digit.Add(new KeyValuePair<string, string>("em{", "र्झ"));
            //List3Digit.Add(new KeyValuePair<string, string>(")f{", "र्ण"));
            //List3Digit.Add(new KeyValuePair<string, string>("if{", "र्ष"));
            //List3Digit.Add(new KeyValuePair<string, string>("km{", "र्फ"));
            //List2Digit.Add(new KeyValuePair<string, string>("of{", "र्या"));



            List2Digit.Add(new KeyValuePair<string, string>(")f", "ण"));
            List2Digit.Add(new KeyValuePair<string, string>("if", "ष"));
            List2Digit.Add(new KeyValuePair<string, string>("If", "क्ष"));
            List2Digit.Add(new KeyValuePair<string, string>("em", "झ"));
            List2Digit.Add(new KeyValuePair<string, string>("km", "फ"));
            List2Digit.Add(new KeyValuePair<string, string>("Tt", "त्त"));
            List2Digit.Add(new KeyValuePair<string, string>("w|", "ध्र"));
            List2Digit.Add(new KeyValuePair<string, string>("cf", "आ"));
            List2Digit.Add(new KeyValuePair<string, string>("P]", "ऐ"));
            List2Digit.Add(new KeyValuePair<string, string>("b|", "द्र"));
            List2Digit.Add(new KeyValuePair<string, string>("km", "फ"));
            List2Digit.Add(new KeyValuePair<string, string>("pm", "ऊ"));

            //List2Digit.Add(new KeyValuePair<string, string>("s{", "र्क"));
            //List2Digit.Add(new KeyValuePair<string, string>("v{", "र्ख"));
            //List2Digit.Add(new KeyValuePair<string, string>("u{", "र्ग"));
            //List2Digit.Add(new KeyValuePair<string, string>("#{", "र्घ"));
            //List2Digit.Add(new KeyValuePair<string, string>("r{", "र्च"));
            //List2Digit.Add(new KeyValuePair<string, string>("%{", "र्छ"));
            //List2Digit.Add(new KeyValuePair<string, string>("h{", "र्ज"));
            //List2Digit.Add(new KeyValuePair<string, string>("^{", "र्ट"));
            //List2Digit.Add(new KeyValuePair<string, string>("&{", "र्ठ"));
            //List2Digit.Add(new KeyValuePair<string, string>("*{", "र्ड"));
            //List2Digit.Add(new KeyValuePair<string, string>("({", "र्ढ"));
            //List2Digit.Add(new KeyValuePair<string, string>("t{", "र्त"));
            //List2Digit.Add(new KeyValuePair<string, string>("y{", "र्थ"));
            //List2Digit.Add(new KeyValuePair<string, string>("b{", "र्द"));
            //List2Digit.Add(new KeyValuePair<string, string>("w{", "र्ध"));
            //List2Digit.Add(new KeyValuePair<string, string>("g{", "र्न"));
            //List2Digit.Add(new KeyValuePair<string, string>("k{", "र्प"));
            //List2Digit.Add(new KeyValuePair<string, string>("a{", "र्ब"));
            //List2Digit.Add(new KeyValuePair<string, string>("e{", "र्भ"));
            //List2Digit.Add(new KeyValuePair<string, string>("d{", "र्म"));
            //List2Digit.Add(new KeyValuePair<string, string>("n{", "र्ल"));
            //List2Digit.Add(new KeyValuePair<string, string>("j{", "र्व"));
            //List2Digit.Add(new KeyValuePair<string, string>("z{", "र्श"));
            //List2Digit.Add(new KeyValuePair<string, string>(";{", "र्स"));
            //List2Digit.Add(new KeyValuePair<string, string>("!{", "र्ज्ञ"));
            //List2Digit.Add(new KeyValuePair<string, string>("k|", "प्र"));
            //List2Digit.Add(new KeyValuePair<string, string>("h|", "ज्र"));
            //List2Digit.Add(new KeyValuePair<string, string>("o{", "र्य"));



            rep.Add(new KeyValuePair<string, string>("ç", "ॐ"));
            rep.Add(new KeyValuePair<string, string>("=", "."));
            //rep.Add(new KeyValuePair<string, string>("B", "द्"));
            rep.Add(new KeyValuePair<string, string>("C", "ऋ"));
            rep.Add(new KeyValuePair<string, string>("M", "।"));
            rep.Add(new KeyValuePair<string, string>("O", "इ"));
            rep.Add(new KeyValuePair<string, string>("P", "ए"));
            rep.Add(new KeyValuePair<string, string>("Q", "त्त"));

            //a-z
            rep.Add(new KeyValuePair<string, string>("a", "ब"));
            rep.Add(new KeyValuePair<string, string>("b", "द"));
            rep.Add(new KeyValuePair<string, string>("c", "अ"));
            rep.Add(new KeyValuePair<string, string>("d", "म"));
            rep.Add(new KeyValuePair<string, string>("e", "भ"));

            rep.Add(new KeyValuePair<string, string>("g", "न"));
            rep.Add(new KeyValuePair<string, string>("h", "ज"));
            rep.Add(new KeyValuePair<string, string>("i", "ष्"));
            rep.Add(new KeyValuePair<string, string>("j", "व"));
            rep.Add(new KeyValuePair<string, string>("k", "प"));
            //rep.Add(new KeyValuePair<string, string>("m", "m"));
            rep.Add(new KeyValuePair<string, string>("n", "ल"));
            rep.Add(new KeyValuePair<string, string>("o", "य"));
            rep.Add(new KeyValuePair<string, string>("p", "उ"));
            rep.Add(new KeyValuePair<string, string>("q", "त्र"));
            rep.Add(new KeyValuePair<string, string>("r", "च"));
            ListFirstPass.Add(new KeyValuePair<string, string>("s", "क"));
            rep.Add(new KeyValuePair<string, string>("t", "त"));
            rep.Add(new KeyValuePair<string, string>("u", "ग"));
            rep.Add(new KeyValuePair<string, string>("v", "ख"));
            rep.Add(new KeyValuePair<string, string>("w", "ध"));
            rep.Add(new KeyValuePair<string, string>("x", "ह"));
            rep.Add(new KeyValuePair<string, string>("y", "थ"));
            rep.Add(new KeyValuePair<string, string>("z", "श"));

            //0-9
            rep.Add(new KeyValuePair<string, string>("0", "०"));
            rep.Add(new KeyValuePair<string, string>("1", "१"));
            rep.Add(new KeyValuePair<string, string>("2", "२"));
            rep.Add(new KeyValuePair<string, string>("3", "३"));
            rep.Add(new KeyValuePair<string, string>("4", "४"));
            rep.Add(new KeyValuePair<string, string>("5", "५"));
            rep.Add(new KeyValuePair<string, string>("6", "६"));
            rep.Add(new KeyValuePair<string, string>("7", "७"));
            rep.Add(new KeyValuePair<string, string>("8", "८"));
            rep.Add(new KeyValuePair<string, string>("9", "९"));

            rep.Add(new KeyValuePair<string, string>("!", "ज्ञ"));

            rep.Add(new KeyValuePair<string, string>(">", "श्र"));
            rep.Add(new KeyValuePair<string, string>("?", "रु"));
            rep.Add(new KeyValuePair<string, string>("Â", "हृ"));
            rep.Add(new KeyValuePair<string, string>("*", "ड़"));
            rep.Add(new KeyValuePair<string, string>("(", "ढ़"));
            rep.Add(new KeyValuePair<string, string>("#", "घ"));
            rep.Add(new KeyValuePair<string, string>("ª", "ङ"));
            rep.Add(new KeyValuePair<string, string>("%", "छ"));
            rep.Add(new KeyValuePair<string, string>("`", "ञ"));
            rep.Add(new KeyValuePair<string, string>("^", "ट"));
            rep.Add(new KeyValuePair<string, string>("&", "ठ"));
            rep.Add(new KeyValuePair<string, string>("*", "ड"));
            rep.Add(new KeyValuePair<string, string>("(", "ढ"));
            rep.Add(new KeyValuePair<string, string>("<", "र"));
            rep.Add(new KeyValuePair<string, string>(";", "स"));
            rep.Add(new KeyValuePair<string, string>("Ÿ", ":"));

            rep.Add(new KeyValuePair<string, string>("=", "."));
            rep.Add(new KeyValuePair<string, string>("º", ")"));
            rep.Add(new KeyValuePair<string, string>("Ö", "="));
            rep.Add(new KeyValuePair<string, string>("Ù", ";"));
            rep.Add(new KeyValuePair<string, string>("…", "‘"));
            rep.Add(new KeyValuePair<string, string>("Ú", "’"));
            rep.Add(new KeyValuePair<string, string>("È", "!"));
            rep.Add(new KeyValuePair<string, string>("Ü", "%"));
            rep.Add(new KeyValuePair<string, string>("æ", "“"));
            rep.Add(new KeyValuePair<string, string>("Æ", "”"));
            rep.Add(new KeyValuePair<string, string>("®", "+"));
            rep.Add(new KeyValuePair<string, string>("œ", "("));
            rep.Add(new KeyValuePair<string, string>("Œ", "?"));
            rep.Add(new KeyValuePair<string, string>("÷", "/"));

        }
    }
}
