﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NepaliToUnicodeConverter
{
    class ShangrilaNumericToUnicode
    {
        string modified_substring = "";
        int array_one_length;
        List<KeyValuePair<string, string>> KeyList = new List<KeyValuePair<string, string>>();

        private void init()
        {

            KeyList.Add(new KeyValuePair<string, string>("˜", "ऽ"));
            KeyList.Add(new KeyValuePair<string, string>("'m", "m'"));
            KeyList.Add(new KeyValuePair<string, string>("]m", "m]"));
            KeyList.Add(new KeyValuePair<string, string>("Fmf", "mfF"));
            KeyList.Add(new KeyValuePair<string, string>("Fm", "mF"));

            KeyList.Add(new KeyValuePair<string, string>("0", "०"));
            KeyList.Add(new KeyValuePair<string, string>("1", "१"));
            KeyList.Add(new KeyValuePair<string, string>("2", "२"));
            KeyList.Add(new KeyValuePair<string, string>("3", "३"));
            KeyList.Add(new KeyValuePair<string, string>("4", "४"));
            KeyList.Add(new KeyValuePair<string, string>("5", "५"));
            KeyList.Add(new KeyValuePair<string, string>("6", "६"));
            KeyList.Add(new KeyValuePair<string, string>("7", "७"));
            KeyList.Add(new KeyValuePair<string, string>("8", "८"));
            KeyList.Add(new KeyValuePair<string, string>("9", "९"));
            KeyList.Add(new KeyValuePair<string, string>("~", "ञ्"));
            KeyList.Add(new KeyValuePair<string, string>(":", "स्"));
            KeyList.Add(new KeyValuePair<string, string>("?", "रु"));
            KeyList.Add(new KeyValuePair<string, string>("ç", "ॐ"));
            KeyList.Add(new KeyValuePair<string, string>("=", "."));
            KeyList.Add(new KeyValuePair<string, string>("em", "झ"));
            KeyList.Add(new KeyValuePair<string, string>("km", "फ"));
            KeyList.Add(new KeyValuePair<string, string>("Qm", "क्त"));
            KeyList.Add(new KeyValuePair<string, string>("St", "क्त"));
            KeyList.Add(new KeyValuePair<string, string>("qm", "क्र"));
            KeyList.Add(new KeyValuePair<string, string>("s|", "क्र"));
            KeyList.Add(new KeyValuePair<string, string>("n", "ल"));
            KeyList.Add(new KeyValuePair<string, string>(@"!\", "ज्ञ्"));
            KeyList.Add(new KeyValuePair<string, string>(@"b\#", "द्घ"));
            KeyList.Add(new KeyValuePair<string, string>("!", "ज्ञ"));
            KeyList.Add(new KeyValuePair<string, string>("$", "द्ध"));
            KeyList.Add(new KeyValuePair<string, string>("¢", "द्ध"));
            KeyList.Add(new KeyValuePair<string, string>(">", "श्र"));
            KeyList.Add(new KeyValuePair<string, string>("?", "रु"));
            KeyList.Add(new KeyValuePair<string, string>("B", "द्य"));
            KeyList.Add(new KeyValuePair<string, string>("I", "क्ष्"));
            KeyList.Add(new KeyValuePair<string, string>("Q", "त्त"));
            KeyList.Add(new KeyValuePair<string, string>("q", "त्र"));
            KeyList.Add(new KeyValuePair<string, string>("w|", "ध्र"));
            KeyList.Add(new KeyValuePair<string, string>("‹", "ङ्घ"));
            KeyList.Add(new KeyValuePair<string, string>(@"*\*", "ड्ड"));
            KeyList.Add(new KeyValuePair<string, string>("b|", "द्र"));
            KeyList.Add(new KeyValuePair<string, string>("§", "ट्ट"));
            KeyList.Add(new KeyValuePair<string, string>(@"*\(", "ड्ढ"));
            KeyList.Add(new KeyValuePair<string, string>("¶", "ठ्ठ"));
            KeyList.Add(new KeyValuePair<string, string>("Â", "हृ"));
            KeyList.Add(new KeyValuePair<string, string>("Ë", "ङ्ग"));
            KeyList.Add(new KeyValuePair<string, string>("Í", "ङ्क"));
            KeyList.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            KeyList.Add(new KeyValuePair<string, string>("Ý", "ट्ठ"));
            KeyList.Add(new KeyValuePair<string, string>("å", "द्व"));
            KeyList.Add(new KeyValuePair<string, string>("^«", "ट्र"));
            KeyList.Add(new KeyValuePair<string, string>("&«", "ठ्र"));
            KeyList.Add(new KeyValuePair<string, string>("*«", "ड्र"));
            KeyList.Add(new KeyValuePair<string, string>("(«", "ढ्र"));
            KeyList.Add(new KeyValuePair<string, string>("|", "्र"));
            KeyList.Add(new KeyValuePair<string, string>("*", "ड़"));
            KeyList.Add(new KeyValuePair<string, string>("(", "ढ़"));
            KeyList.Add(new KeyValuePair<string, string>(@"s\", "क्"));
            KeyList.Add(new KeyValuePair<string, string>("S", "क्"));
            KeyList.Add(new KeyValuePair<string, string>("s", "क"));
            KeyList.Add(new KeyValuePair<string, string>(@"v\", "ख्"));
            KeyList.Add(new KeyValuePair<string, string>("V", "ख्"));
            KeyList.Add(new KeyValuePair<string, string>("v", "ख"));
            KeyList.Add(new KeyValuePair<string, string>(@"u\", "ग्"));
            KeyList.Add(new KeyValuePair<string, string>("U", "ग्"));
            KeyList.Add(new KeyValuePair<string, string>("u", "ग"));
            KeyList.Add(new KeyValuePair<string, string>(@"#\", "घ्"));
            KeyList.Add(new KeyValuePair<string, string>("#", "घ"));
            KeyList.Add(new KeyValuePair<string, string>("ª", "ङ"));
            KeyList.Add(new KeyValuePair<string, string>(@"r\", "च्"));
            KeyList.Add(new KeyValuePair<string, string>("R", "च्"));
            KeyList.Add(new KeyValuePair<string, string>("r", "च"));
            KeyList.Add(new KeyValuePair<string, string>("%", "छ"));
            KeyList.Add(new KeyValuePair<string, string>(@"h\", "ज्"));
            KeyList.Add(new KeyValuePair<string, string>("H", "ज्"));
            KeyList.Add(new KeyValuePair<string, string>("h", "ज"));
            KeyList.Add(new KeyValuePair<string, string>(@"em\", "झ्"));
            KeyList.Add(new KeyValuePair<string, string>(@"`\", "ञ्"));
            KeyList.Add(new KeyValuePair<string, string>("`", "ञ"));
            KeyList.Add(new KeyValuePair<string, string>("^", "ट"));
            KeyList.Add(new KeyValuePair<string, string>("&", "ठ"));
            KeyList.Add(new KeyValuePair<string, string>("*", "ड"));
            KeyList.Add(new KeyValuePair<string, string>("(", "ढ"));
            KeyList.Add(new KeyValuePair<string, string>(")", "ण्"));
            KeyList.Add(new KeyValuePair<string, string>(@"t\", "त्"));
            KeyList.Add(new KeyValuePair<string, string>("t", "त"));
            KeyList.Add(new KeyValuePair<string, string>(@"y\", "थ्"));
            KeyList.Add(new KeyValuePair<string, string>("Y", "थ्"));
            KeyList.Add(new KeyValuePair<string, string>("y", "थ"));
            KeyList.Add(new KeyValuePair<string, string>("b", "द"));
            KeyList.Add(new KeyValuePair<string, string>(@"w\", "ध्"));
            KeyList.Add(new KeyValuePair<string, string>("W", "ध्"));
            KeyList.Add(new KeyValuePair<string, string>("w", "ध"));
            KeyList.Add(new KeyValuePair<string, string>(@"g\", "न्"));
            KeyList.Add(new KeyValuePair<string, string>("G", "न्"));
            KeyList.Add(new KeyValuePair<string, string>("g", "न"));
            KeyList.Add(new KeyValuePair<string, string>(@"k\", "प्"));
            KeyList.Add(new KeyValuePair<string, string>(@"K", "प्"));
            KeyList.Add(new KeyValuePair<string, string>("k", "प"));
            KeyList.Add(new KeyValuePair<string, string>(@"km\", "फ्"));
            KeyList.Add(new KeyValuePair<string, string>("ˆ", "फ्"));
            KeyList.Add(new KeyValuePair<string, string>(@"a\", "ब्"));
            KeyList.Add(new KeyValuePair<string, string>("A", "ब्"));
            KeyList.Add(new KeyValuePair<string, string>("a", "ब"));
            KeyList.Add(new KeyValuePair<string, string>(@"e\", "भ्"));
            KeyList.Add(new KeyValuePair<string, string>("E", "भ्"));
            KeyList.Add(new KeyValuePair<string, string>("e", "भ"));
            KeyList.Add(new KeyValuePair<string, string>(@"d\", "म्"));
            KeyList.Add(new KeyValuePair<string, string>("D", "म्"));
            KeyList.Add(new KeyValuePair<string, string>("d", "म"));
            KeyList.Add(new KeyValuePair<string, string>("o", "य"));
            KeyList.Add(new KeyValuePair<string, string>("<", "र"));
            KeyList.Add(new KeyValuePair<string, string>(@"n\", "ल्"));
            KeyList.Add(new KeyValuePair<string, string>("N", "ल्"));
            KeyList.Add(new KeyValuePair<string, string>(@"j\", "व्"));
            KeyList.Add(new KeyValuePair<string, string>("J", "व्"));
            KeyList.Add(new KeyValuePair<string, string>("j", "व"));
            KeyList.Add(new KeyValuePair<string, string>(@"z\", "श्"));
            KeyList.Add(new KeyValuePair<string, string>("Z", "श्"));
            KeyList.Add(new KeyValuePair<string, string>("z", "श"));
            KeyList.Add(new KeyValuePair<string, string>("i", "ष्"));
            KeyList.Add(new KeyValuePair<string, string>(@";\", "स्"));
            KeyList.Add(new KeyValuePair<string, string>(";", "स"));
            KeyList.Add(new KeyValuePair<string, string>(@"x\", "ह्"));
            KeyList.Add(new KeyValuePair<string, string>("X", "ह्"));
            KeyList.Add(new KeyValuePair<string, string>("x", "ह"));
            KeyList.Add(new KeyValuePair<string, string>("cfF", "ऑ"));
            KeyList.Add(new KeyValuePair<string, string>("cf}", "औ"));
            KeyList.Add(new KeyValuePair<string, string>("cf]", "ओ"));
            KeyList.Add(new KeyValuePair<string, string>("cf", "आ"));
            KeyList.Add(new KeyValuePair<string, string>("c", "अ"));
            KeyList.Add(new KeyValuePair<string, string>("O{", "ई"));
            KeyList.Add(new KeyValuePair<string, string>("O", "इ"));
            KeyList.Add(new KeyValuePair<string, string>("pm", "ऊ"));
            KeyList.Add(new KeyValuePair<string, string>("p", "उ"));
            KeyList.Add(new KeyValuePair<string, string>("C", "ऋ"));
            KeyList.Add(new KeyValuePair<string, string>("P]", "ऐ"));
            KeyList.Add(new KeyValuePair<string, string>("P", "ए"));
            KeyList.Add(new KeyValuePair<string, string>("{", "र्"));//up
            KeyList.Add(new KeyValuePair<string, string>("\"", "ू"));
            KeyList.Add(new KeyValuePair<string, string>("'", "ु"));
            KeyList.Add(new KeyValuePair<string, string>("_", "ं"));
            KeyList.Add(new KeyValuePair<string, string>("f", "ा"));
            KeyList.Add(new KeyValuePair<string, string>("[", "ृ")); //DOWN
            KeyList.Add(new KeyValuePair<string, string>(@"\", "्"));
            KeyList.Add(new KeyValuePair<string, string>("]", "े"));
            KeyList.Add(new KeyValuePair<string, string>("}", "ै"));
            KeyList.Add(new KeyValuePair<string, string>("F", "ँ"));
            KeyList.Add(new KeyValuePair<string, string>("L", "ी"));
            KeyList.Add(new KeyValuePair<string, string>("Ÿ", "ः"));
            KeyList.Add(new KeyValuePair<string, string>("्ा", "ः"));
            KeyList.Add(new KeyValuePair<string, string>("f}", "ौ"));
            KeyList.Add(new KeyValuePair<string, string>("f]", "ो"));
            KeyList.Add(new KeyValuePair<string, string>("l", "ि"));
            KeyList.Add(new KeyValuePair<string, string>("्ो", "े"));
            KeyList.Add(new KeyValuePair<string, string>("्ौ", "ै"));
            KeyList.Add(new KeyValuePair<string, string>("अो", "ओ"));
            KeyList.Add(new KeyValuePair<string, string>("अा", "आ"));
            KeyList.Add(new KeyValuePair<string, string>("आै", "औ"));
            KeyList.Add(new KeyValuePair<string, string>("आे", "ओ"));
            KeyList.Add(new KeyValuePair<string, string>("ाो", "ो"));
            KeyList.Add(new KeyValuePair<string, string>("ाॅ", "ॉ"));
            KeyList.Add(new KeyValuePair<string, string>("ाे", "ो"));
            KeyList.Add(new KeyValuePair<string, string>("ंु", "ुं"));
            KeyList.Add(new KeyValuePair<string, string>("ेे", "े"));
            KeyList.Add(new KeyValuePair<string, string>("ाे", "ो"));
            KeyList.Add(new KeyValuePair<string, string>("ंा", "ां"));
            KeyList.Add(new KeyValuePair<string, string>("ाै", "ौ"));
            KeyList.Add(new KeyValuePair<string, string>("ैा", "ौ"));
            KeyList.Add(new KeyValuePair<string, string>("ंृ", "ृं"));
            KeyList.Add(new KeyValuePair<string, string>("ँा", "ाँ"));
            KeyList.Add(new KeyValuePair<string, string>("ँू", "ूँ"));
            KeyList.Add(new KeyValuePair<string, string>("ेा", "ो"));
            KeyList.Add(new KeyValuePair<string, string>("ंे", "ें"));
            KeyList.Add(new KeyValuePair<string, string>("M", "।"));
            KeyList.Add(new KeyValuePair<string, string>("sL{", "र्की"));
           
            KeyList.Add(new KeyValuePair<string, string>(@"b\o", "द्य"));
            KeyList.Add(new KeyValuePair<string, string>("्ो", "े"));
            KeyList.Add(new KeyValuePair<string, string>("ेे", "े"));
            
            //old
           /*
            KeyList.Add(new KeyValuePair<string, string>("em{", "र्झ"));
            KeyList.Add(new KeyValuePair<string, string>(")f{", "र्ण"));
            KeyList.Add(new KeyValuePair<string, string>("if{", "र्ष"));
            KeyList.Add(new KeyValuePair<string, string>("km{", "र्फ"));
            KeyList.Add(new KeyValuePair<string, string>(@"o\", "य्"));
            KeyList.Add(new KeyValuePair<string, string>(@"<\", "र्"));
            KeyList.Add(new KeyValuePair<string, string>(@"Tt\", "त्त्"));
            KeyList.Add(new KeyValuePair<string, string>(@"q\", "त्र्"));
            KeyList.Add(new KeyValuePair<string, string>(@">\", "श्र्"));
            KeyList.Add(new KeyValuePair<string, string>("D", "म्"));
            KeyList.Add(new KeyValuePair<string, string>("E", "भ्"));
            KeyList.Add(new KeyValuePair<string, string>("G", "न्"));
            KeyList.Add(new KeyValuePair<string, string>("J", "व्"));
            KeyList.Add(new KeyValuePair<string, string>("K", "प्"));
            KeyList.Add(new KeyValuePair<string, string>("N", "ल्"));
            KeyList.Add(new KeyValuePair<string, string>("S", "क्"));
            KeyList.Add(new KeyValuePair<string, string>("T", "त्"));
            KeyList.Add(new KeyValuePair<string, string>("W", "ध्"));
            KeyList.Add(new KeyValuePair<string, string>("X", "ह्"));
            KeyList.Add(new KeyValuePair<string, string>("Y", "थ्"));
            KeyList.Add(new KeyValuePair<string, string>("Z", "श्"));
            KeyList.Add(new KeyValuePair<string, string>(@"If\", "क्ष्"));
            
            KeyList.Add(new KeyValuePair<string, string>(@"if\", "ष्"));
            KeyList.Add(new KeyValuePair<string, string>("b|", "द्र"));
            KeyList.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            KeyList.Add(new KeyValuePair<string, string>("j", "व"));
            KeyList.Add(new KeyValuePair<string, string>("z", "श"));
            KeyList.Add(new KeyValuePair<string, string>("=", "."));
            KeyList.Add(new KeyValuePair<string, string>("º", ")"));
            KeyList.Add(new KeyValuePair<string, string>("Ö", "="));
            KeyList.Add(new KeyValuePair<string, string>("Ù", ";"));
            KeyList.Add(new KeyValuePair<string, string>("…", "‘"));
            KeyList.Add(new KeyValuePair<string, string>("Ú", "’"));
            KeyList.Add(new KeyValuePair<string, string>("È", "!"));
            KeyList.Add(new KeyValuePair<string, string>("Ü", "%"));
            KeyList.Add(new KeyValuePair<string, string>("æ", "“"));
            KeyList.Add(new KeyValuePair<string, string>("Æ", "”"));
            KeyList.Add(new KeyValuePair<string, string>("®", "+"));
            KeyList.Add(new KeyValuePair<string, string>("œ", "("));
            KeyList.Add(new KeyValuePair<string, string>("Œ", "?"));
            KeyList.Add(new KeyValuePair<string, string>("÷", "/"));
            KeyList.Add(new KeyValuePair<string, string>(")f", "ण"));
            //KeyList.Add(new KeyValuePair<string, string>("if", "ष")); //suraj
            
            KeyList.Add(new KeyValuePair<string, string>("em", "झ"));
            KeyList.Add(new KeyValuePair<string, string>("km", "फ"));
            KeyList.Add(new KeyValuePair<string, string>("Tt", "त्त"));
            KeyList.Add(new KeyValuePair<string, string>("km", "फ"));
            
            */
            
            
            
            
            
            
            
          

        }

        public string ShangrilaConfigToUnicode(string legacy_text)
        {

            init();

            legacy_text = ReplaceMixingWords(legacy_text);

            array_one_length = KeyList.Count;


            int text_size = legacy_text.Length;

            string processed_text = "";  //blank

            int sthiti1 = 0;
            int sthiti2 = 0;
            int chale_chalo = 1;

            int max_text_size = 6000;

            while (chale_chalo == 1)
            {
                sthiti1 = sthiti2;

                if (sthiti2 < (text_size - max_text_size))
                {
                    sthiti2 += max_text_size;
                    char scharAt = Convert.ToChar(legacy_text.Substring(sthiti2, 1));

                    while (scharAt != ' ')
                    {
                        sthiti2--;
                    }
                }
                else
                {
                    sthiti2 = text_size;
                    chale_chalo = 0;
                }

                modified_substring = legacy_text.Substring(sthiti1, sthiti2);

                Replace_Symbols();

                processed_text += modified_substring;

            }
            return processed_text;
        }


        
        void Remove()
        {
            //sgf{

            //s{gf

            //lDa

            string stoppedChar = "l{";//{
            string NewShangrila = "";
            var spilitted = modified_substring.Split(' ');

            foreach (var singleWord in spilitted)
            {
                string ProcessWord = singleWord;

                foreach (char stpChar in stoppedChar.ToCharArray())
                {
                    if (ProcessWord.Contains(stpChar.ToString()))
                    {
                        try
                        {
                            char[] array = ProcessWord.ToCharArray();
                            char[] temparray = array;

                            for (int index = 0; index < array.Length; index++)
                            {
                                if (array[index] == stpChar)
                                {
                                    bool skip = false;
                                    char temp = temparray[index]; // Get temporary copy of character
                                    try
                                    {

                                        if ((char.IsUpper(array[index + 1]) || array[index+1]==':') && array[index] == 'l')
                                        {
                                            char zero = temparray[index];
                                            char one = temparray[index+1];
                                            char two = temparray[index + 2];

                                            temparray[index] = one;
                                            temparray[index+1] = two;
                                            temparray[index + 2] = zero;
                                            index = index + 2;
                                            skip = true;
                                        }

                                        if (array[index - 1] == 'f' && array[index] == '{')
                                        {
                                            temparray[index] = temparray[index - 1]; // Assign element
                                            temparray[index - 1] = temparray[index - 2]; // Assign element
                                            temparray[index - 2] = temp; // Assign element
                                            index++;
                                            skip = true;
                                        }
                                    }
                                    catch { }

                                    if(array[index]!='{' && !skip)
                                    {
                                        try
                                        {
                                            temparray[index] = temparray[index + 1]; // Assign element
                                            temparray[index + 1] = temp; // Assign element
                                            index++;
                                        }
                                        catch (Exception)
                                        {

                                            temparray[index] = temparray[index - 1]; // Assign element
                                            temparray[index - 1] = temp; // Assign element
                                        }
                                    }
                                    ProcessWord = new string(temparray);
                                    
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
                NewShangrila += ProcessWord + " ";

            }

            modified_substring = NewShangrila.Trim();
        }

        private static string ReplaceMixingWords(string ShangrilaFont)
        {
            if (ShangrilaFont.Contains("lh{"))
            {
                ShangrilaFont = ShangrilaFont.Replace("lh{", "र्जि");
            }

            if (ShangrilaFont.Contains("sL{"))
            {
                ShangrilaFont = ShangrilaFont.Replace("sL{", "र्की");
            }

            if (ShangrilaFont.Contains("lif"))
            {
                ShangrilaFont = ShangrilaFont.Replace("lif", "षि");
            }

            if (ShangrilaFont.Contains("iff"))

            {
                ShangrilaFont = ShangrilaFont.Replace("iff", "षा");
            }

            if (ShangrilaFont.Contains("l)f"))
            {
                ShangrilaFont = ShangrilaFont.Replace("l)f", "णि");
            }

            if (ShangrilaFont.Contains("o{"))
            {
                ShangrilaFont = ShangrilaFont.Replace("o{", "र्य");
            }

            if (ShangrilaFont.Contains("of{"))
            {
                ShangrilaFont = ShangrilaFont.Replace("of{", "र्या");
            }

            if (ShangrilaFont.Contains(")f"))
            {
                ShangrilaFont = ShangrilaFont.Replace(")f", "ण");
            }

            if (ShangrilaFont.Contains("if"))
            {
                ShangrilaFont = ShangrilaFont.Replace("if", "ष");
            }

            if (ShangrilaFont.Contains("If"))
            {
                ShangrilaFont = ShangrilaFont.Replace("If", "क्ष");
            }





            return ShangrilaFont;
        }


        string ToUnicode(string Shangrila)
        {
            string Unicode = Shangrila;
            foreach (var item in KeyList)
            {
                Unicode = Unicode.Replace(item.Key, item.Value);
            }
            return Unicode;
        }


        void Replace_Symbols()
        {
            if (modified_substring != "")  // if stringto be converted is non-blank then no need of any processing.
            {

                Remove();

                foreach (var item in KeyList)
                {
                        modified_substring = modified_substring.Replace(item.Key, item.Value);
                }
                
                

                //**********************************************************************************
                // End of Code for Replacing four Special glyphs
                //**********************************************************************************

                // following loop to eliminate 'chhotee ee kee maatraa' on half-letters as a result of above transformation.

                int position_of_wrong_ee = modified_substring.IndexOf("ि्");

                while (position_of_wrong_ee != -1)  //while-03
                {
                    string consonent_next_to_wrong_ee = modified_substring.Substring(position_of_wrong_ee + 2);

                    string charecter_to_be_replaced = "ि्" + consonent_next_to_wrong_ee;
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, "्" + consonent_next_to_wrong_ee + "ि");
                    position_of_wrong_ee = modified_substring.IndexOf("/ि्/", position_of_wrong_ee + 2); // search for 'wrong ee' ahead of the current position.

                } // end of while-03 loop

                // following loop to eliminate 'chhotee ee kee maatraa' on half-letters as a result of above transformation.

                position_of_wrong_ee = modified_substring.IndexOf("िं्");

                while (position_of_wrong_ee != -1)  //while-03
                {
                    string consonent_next_to_wrong_ee = modified_substring.Substring(position_of_wrong_ee + 3);

                    //FIXME_VAR_TYPE consonent_next_to_wrong_ee= modified_substring.charAt( position_of_wrong_ee + 3 )

                    string charecter_to_be_replaced = "िं्" + consonent_next_to_wrong_ee;
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, "्" + consonent_next_to_wrong_ee + "िं");
                    position_of_wrong_ee = modified_substring.IndexOf("/िं्/", position_of_wrong_ee + 3); // search for 'wrong ee' ahead of the current position.

                } // end of while-03 loop


                // Eliminating reph "Ô" and putting 'half - r' at proper position for this.
                var set_of_matras = "ा ि ी ु ू ृ े ै ो ौ ं : ँ ॅ";
                int position_of_R = modified_substring.IndexOf("{");

                while (position_of_R > 0)  // while-04
                {
                    var probable_position_of_half_r = position_of_R - 1;
                    string charecter_at_probable_position_of_half_r = modified_substring.Substring(probable_position_of_half_r);

                    // trying to find non-maatra position left to current O (ie, half -r).

                    while (set_of_matras.Contains(charecter_at_probable_position_of_half_r) != false)  // while-05
                    {
                        probable_position_of_half_r = probable_position_of_half_r - 1;
                        charecter_at_probable_position_of_half_r = modified_substring.Substring(probable_position_of_half_r);

                    } // end of while-05


                    var charecter_to_be_replaced = modified_substring.Substring(probable_position_of_half_r, (position_of_R - probable_position_of_half_r));
                    var new_replacement_string = "र्" + charecter_to_be_replaced;
                    charecter_to_be_replaced = charecter_to_be_replaced + "{";
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, new_replacement_string);
                    position_of_R = modified_substring.IndexOf("{");

                } // end of while-04

                // global conversion of punctuation marks
                //    "=","_","Ö","Ù","‘","Ú","Û","Ü","æ","Æ","±","-","<",
                //    ".",")","=", ";","…", "’","!","%","“","”","+","(","?",

                modified_substring = modified_substring.Replace("/=/g", ".");
                modified_substring = modified_substring.Replace("/_/g", ")");
                modified_substring = modified_substring.Replace("/Ö/g", "=");
                modified_substring = modified_substring.Replace("/Ù/g", ";");
                modified_substring = modified_substring.Replace("/…/g", "‘");
                modified_substring = modified_substring.Replace("/Ú/g", "’");
                modified_substring = modified_substring.Replace("/Û/g", "!");
                modified_substring = modified_substring.Replace("/Ü/g", "%");
                modified_substring = modified_substring.Replace("/æ/g", "“");
                modified_substring = modified_substring.Replace("/Æ/g", "”");
                modified_substring = modified_substring.Replace("/±/g", "+");
                modified_substring = modified_substring.Replace("/-/g", "(");
                modified_substring = modified_substring.Replace("/</g", "?");

            } // end of IF  statement  meant to  supress processing of  blank  string.

        } // end of the function  Replace_Symbols

    }
}
