﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace NepaliToUnicodeConverter
{
    class PreetiToUnicode
    {
        string modified_substring = "";
        int array_one_length;
        string[] array_one;
        string[] array_two;
       
        public string convert_to_unicode(string legacy_text)
        {


            array_one = new string[]{

                "ç","˜",".","'m","]m","Fmf","Fm",")","!","@","#","$","%","^","&","*","(","k|m","em","km","Qm","qm","N˜","¡","¢","1","2","4",">","?","B","I","Q","ß",
                "q","„","‹","•","›","§","°","¶","¿","Å","Ë","Ì","Í","Î","Ý","å","6«","7«","8«","9«","Ø","|","8Þ","9Þ","S","s","V","v","U","u","£","3","ª",
                "R","r","5","H","h","‰","´","~", "`","6","7","8","9","0","T","t","Y","y","b","W","w","G","g","K","k","ˆ","A","a","E", "e","D","d",
                "o","/","N","n","J", "j", "Z","z","i",":",";","X","x","cf‘","c‘f","cf}","cf]","cf","c","O{","O","pm","p","C","P]","P",
                "f‘","\"","'","+","f","[","\\","]","}","F", "L","M","्ा","्ो","्ौ","अो","अा","आै","आे","ाो","ाॅ","ाे",
                "ंु","ेे","अै","ाे","अे","ंा","अॅ","ाै","ैा","ंृ","ँा","ँू","ेा","ंे"};

            // Remove typing mistakes in the original file

            //"_","Ö","Ù","Ú","Û","Ü","Þ","Æ","±","-","<","=")    // Punctuation marks

            array_two = new string[]{

                "ॐ","ऽ","।","m'","m]","mfF","mF","०","१","२","३","४","५","६","७","८","९","फ्र   ","झ","फ","क्त","क्र","ल","ज्ञ्","द्घ","ज्ञ","द्द","द्ध","श्र","रु","द्य","क्ष्","त्त","द्म    ",
                "त्र","ध्र","ङ्घ","ड्ड","द्र","ट्ट","ड्ढ","ठ्ठ","रू    ","हृ","ङ्ग","त्र","ङ्क","ङ्ख","ट्ठ","द्व","ट्र","ठ्र","ड्र","ढ्र","्य    ","्र","ड़","ढ़","क्","क","ख्","ख","ग्","ग","घ्","घ", "ङ",
                "च्","च","छ","ज्","ज","झ्","झ","ञ्", "ञ","ट","ठ","ड","ढ","ण्","त्","त","थ्","थ","द","ध्","ध","न्","न","प्","प","फ्","ब्","ब","भ्","भ","म्","म",
                "य","र","ल्","ल","व्","व","श्","श","ष्","स्","स","ह्","ह","ऑ","ऑ","औ","ओ","आ","अ","ई","इ","ऊ","उ","ऋ","ऐ","ए",
                "ॉ","ू","ु","ं","ा","ृ","्","े","ै","ँ","ी","ः","","े","ै","ओ","आ","औ","ओ","ो","ॉ","ो","ुं","े","अ‍ै","ो","अ‍े","ां","अ‍ॅ","ौ","ौ","ृं","ाँ","ूँ","ो","ें"};

            array_one_length = array_one.Length;


            int text_size = legacy_text.Length;

            string processed_text = "";  //blank

            int sthiti1 = 0;
            int sthiti2 = 0;
            int chale_chalo = 1;

            int max_text_size = 6000;

            while (chale_chalo == 1)
            {
                sthiti1 = sthiti2;

                if (sthiti2 < (text_size - max_text_size))
                {
                    sthiti2 += max_text_size;
                    char scharAt = Convert.ToChar(legacy_text.Substring(sthiti2, 1));

                    while (scharAt != ' ')
                    {
                        sthiti2--;
                    }
                }
                else
                {
                    sthiti2 = text_size;
                    chale_chalo = 0;
                }

                modified_substring = legacy_text.Substring(sthiti1, sthiti2);

                Replace_Symbols();

                processed_text += modified_substring;
              
            }
            return processed_text;
        }

        void Replace_Symbols()
        {
            if (modified_substring != "")  // if stringto be converted is non-blank then no need of any processing.
            {
                for (int input_symbol_idx = 0; input_symbol_idx < array_one_length; input_symbol_idx++)
                {

                    int idx = 0;  // index of the symbol being searched for replacement

                    while (idx != -1) //while-00
                    {

                        modified_substring = modified_substring.Replace(array_one[input_symbol_idx], array_two[input_symbol_idx]);
                        idx = modified_substring.IndexOf(array_one[input_symbol_idx]);

                    } // end of while-00 loop
                    // alert(" end of while loop")
                } // end of for loop
                // alert(" end of for loop")

                //*******************************************************
                int position_of_i = modified_substring.IndexOf("l");

                while (position_of_i != -1)  //while-02
                {
                    string charecter_next_to_i = modified_substring.Substring(position_of_i + 1);

                    string charecter_to_be_replaced = "l" + charecter_next_to_i;

                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, charecter_next_to_i + "ि");
                    position_of_i = modified_substring.IndexOf('l', position_of_i + 1); // search for i ahead of the current position.

                } // end of while-02 loop

                //**********************************************************************************
                // End of Code for Replacing four Special glyphs
                //**********************************************************************************

                // following loop to eliminate 'chhotee ee kee maatraa' on half-letters as a result of above transformation.

                int position_of_wrong_ee = modified_substring.IndexOf("ि्");

                while (position_of_wrong_ee != -1)  //while-03
                {
                    string consonent_next_to_wrong_ee = modified_substring.Substring(position_of_wrong_ee + 2);

                    string charecter_to_be_replaced = "ि्" + consonent_next_to_wrong_ee;
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, "्" + consonent_next_to_wrong_ee + "ि");
                    position_of_wrong_ee = modified_substring.IndexOf("/ि्/", position_of_wrong_ee + 2); // search for 'wrong ee' ahead of the current position.

                } // end of while-03 loop

                // following loop to eliminate 'chhotee ee kee maatraa' on half-letters as a result of above transformation.

                position_of_wrong_ee = modified_substring.IndexOf("िं्");

                while (position_of_wrong_ee != -1)  //while-03
                {
                    string consonent_next_to_wrong_ee = modified_substring.Substring(position_of_wrong_ee + 3);

                    //FIXME_VAR_TYPE consonent_next_to_wrong_ee= modified_substring.charAt( position_of_wrong_ee + 3 )

                    string charecter_to_be_replaced = "िं्" + consonent_next_to_wrong_ee;
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, "्" + consonent_next_to_wrong_ee + "िं");
                    position_of_wrong_ee = modified_substring.IndexOf("/िं्/", position_of_wrong_ee + 3); // search for 'wrong ee' ahead of the current position.

                } // end of while-03 loop


                // Eliminating reph "Ô" and putting 'half - r' at proper position for this.
                var set_of_matras = "ा ि ी ु ू ृ े ै ो ौ ं : ँ ॅ";
                int position_of_R = modified_substring.IndexOf("{");

                while (position_of_R > 0)  // while-04
                {
                    var probable_position_of_half_r = position_of_R - 1;
                    string charecter_at_probable_position_of_half_r = modified_substring.Substring(probable_position_of_half_r);

                    // trying to find non-maatra position left to current O (ie, half -r).

                    while (set_of_matras.Contains(charecter_at_probable_position_of_half_r) != false)  // while-05
                    {
                        probable_position_of_half_r = probable_position_of_half_r - 1;
                        charecter_at_probable_position_of_half_r = modified_substring.Substring(probable_position_of_half_r);

                    } // end of while-05


                    var charecter_to_be_replaced = modified_substring.Substring(probable_position_of_half_r, (position_of_R - probable_position_of_half_r));
                    var new_replacement_string = "र्" + charecter_to_be_replaced;
                    charecter_to_be_replaced = charecter_to_be_replaced + "{";
                    modified_substring = modified_substring.Replace(charecter_to_be_replaced, new_replacement_string);
                    position_of_R = modified_substring.IndexOf("{");

                } // end of while-04

                // global conversion of punctuation marks
                //    "=","_","Ö","Ù","‘","Ú","Û","Ü","æ","Æ","±","-","<",
                //    ".",")","=", ";","…", "’","!","%","“","”","+","(","?",

                modified_substring = modified_substring.Replace("/=/g", ".");
                modified_substring = modified_substring.Replace("/_/g", ")");
                modified_substring = modified_substring.Replace("/Ö/g", "=");
                modified_substring = modified_substring.Replace("/Ù/g", ";");
                modified_substring = modified_substring.Replace("/…/g", "‘");
                modified_substring = modified_substring.Replace("/Ú/g", "’");
                modified_substring = modified_substring.Replace("/Û/g", "!");
                modified_substring = modified_substring.Replace("/Ü/g", "%");
                modified_substring = modified_substring.Replace("/æ/g", "“");
                modified_substring = modified_substring.Replace("/Æ/g", "”");
                modified_substring = modified_substring.Replace("/±/g", "+");
                modified_substring = modified_substring.Replace("/-/g", "(");
                modified_substring = modified_substring.Replace("/</g", "?");

            } // end of IF  statement  meant to  supress processing of  blank  string.

        } // end of the function  Replace_Symbols

    }
}
