﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NepaliToUnicodeConverter
{
    class WordFileManager
    {
       public void Read(string path)
        {
            StringBuilder strBuilder = new StringBuilder();
             FileStream fStream = File.Create(@"f:\suraj.doc");
            fStream.Close();
            StreamWriter sWriter = new StreamWriter(@"f:\suraj.doc");
           

            Application word = new Application();
            Document doc = new Document();

            object fileName = path;
            // Define an object to pass to the API for missing parameters
            object missing = System.Type.Missing;
            doc = word.Documents.Open(ref fileName,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing);

            String read = string.Empty;
            List<string> data = new List<string>();
            for (int i = 0; i < doc.Paragraphs.Count; i++)
            {
                string temp = doc.Paragraphs[i + 1].Range.Text.Trim();
                if (temp != string.Empty)
                {
                    PreetiToUnicode p = new PreetiToUnicode();
                    string UniCode = p.convert_to_unicode(temp);
                    sWriter.Write(UniCode);
                   
                }
            }
            ((_Document)doc).Close();
            ((_Application)word).Quit();
            sWriter.Close();
        }
    }
}
