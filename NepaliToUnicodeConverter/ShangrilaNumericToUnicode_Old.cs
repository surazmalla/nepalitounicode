﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NepaliToUnicodeConverter
{
    class ShangrilaNumericToUnicode_Old
    {
        
        //List<KeyValuePair<string, string>> List2Digit = new List<KeyValuePair<string, string>>();

        List<KeyValuePair<string, string>> ListFirstPass = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ListSecondPass = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ListThirdPass = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ListFinalPass = new List<KeyValuePair<string, string>>();
        

        private void init()
        {
            //FIRST PASS

            //"",".","","","",""
            //"","","","","",""

            ListFirstPass.Add(new KeyValuePair<string, string>("˜", "ऽ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(".", "।"));
            ListFirstPass.Add(new KeyValuePair<string, string>("'m", "m'"));
            ListFirstPass.Add(new KeyValuePair<string, string>("]m", "m]"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Fmf", "mfF"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Fm", "mF"));


            ListFirstPass.Add(new KeyValuePair<string, string>(@"cfF", "ऑ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"cf}", "औ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"cf]", "ओ"));
            ListFirstPass.Add(new KeyValuePair<string, string>("sL{", "र्की"));
            ListFirstPass.Add(new KeyValuePair<string, string>("em{", "र्झ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(")f{", "र्ण"));
            ListFirstPass.Add(new KeyValuePair<string, string>("if{", "र्ष"));
            ListFirstPass.Add(new KeyValuePair<string, string>("km{", "र्फ"));
            ListFirstPass.Add(new KeyValuePair<string, string>("of{", "र्या"));
            ListFirstPass.Add(new KeyValuePair<string, string>("O{", "ई"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"o\", "य्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"<\", "र्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"km\", "फ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"Tt\", "त्त्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"em\", "झ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"s\", "क्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"v\", "ख्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"u\", "ग्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"#\", "घ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"r\", "च्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"h\", "ज्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"em", "झ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"`\", "ञ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"t\", "त्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"y\", "थ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"w\", "ध्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"g\", "न्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"k\", "प्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"a\", "ब्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"e\", "भ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"d\", "म्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"n\", "ल्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"j\", "व्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"z\", "श्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@";\", "स्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"x\", "ह्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"!\", "ज्ञ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"q\", "त्र्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@">\", "श्र्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("ˆ", "फ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(")", "ण्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("A", "ब्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("B", "द्य"));
            ListFirstPass.Add(new KeyValuePair<string, string>("D", "म्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("E", "भ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("G", "न्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("H", "ज्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("I", "क्ष्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("J", "व्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("K", "प्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("N", "ल्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("R", "च्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("S", "क्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("T", "त्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("U", "ग्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("V", "ख्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("W", "ध्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("X", "ह्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Y", "थ्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Z", "श्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"*\(", "ड्ढ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"If\", "क्ष्"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"b\#", "द्घ"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"b\o", "द्य"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"*\*", "ड्ड"));
            ListFirstPass.Add(new KeyValuePair<string, string>(@"if\", "ष्"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Qm", "क्त"));
            ListFirstPass.Add(new KeyValuePair<string, string>("qm", "क्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("St", "क्त"));
            ListFirstPass.Add(new KeyValuePair<string, string>("s|", "क्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("b|", "द्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("^«", "ट्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("&«", "ठ्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("*«", "ड्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("(«", "ढ्र"));
            ListFirstPass.Add(new KeyValuePair<string, string>("$", "द्द"));
            ListFirstPass.Add(new KeyValuePair<string, string>("¢", "द्ध"));
            ListFirstPass.Add(new KeyValuePair<string, string>("‹", "ङ्घ"));
            ListFirstPass.Add(new KeyValuePair<string, string>("§", "ट्ट"));
            ListFirstPass.Add(new KeyValuePair<string, string>("¶", "ठ्ठ"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Ë", "ङ्ग"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Í", "ङ्क"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Ý", "ट्ठ"));
            ListFirstPass.Add(new KeyValuePair<string, string>("å", "द्व"));
            ListFirstPass.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            ListFirstPass.Add(new KeyValuePair<string, string>("0", "०"));
            ListFirstPass.Add(new KeyValuePair<string, string>("1", "१"));
            ListFirstPass.Add(new KeyValuePair<string, string>("2", "२"));
            ListFirstPass.Add(new KeyValuePair<string, string>("3", "३"));
            ListFirstPass.Add(new KeyValuePair<string, string>("4", "४"));
            ListFirstPass.Add(new KeyValuePair<string, string>("5", "५"));
            ListFirstPass.Add(new KeyValuePair<string, string>("6", "६"));
            ListFirstPass.Add(new KeyValuePair<string, string>("7", "७"));
            ListFirstPass.Add(new KeyValuePair<string, string>("8", "८"));
            ListFirstPass.Add(new KeyValuePair<string, string>("9", "९"));


            //SECOND PASS
            ListSecondPass.Add(new KeyValuePair<string, string>("ç", "ॐ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("=", "."));
            ListSecondPass.Add(new KeyValuePair<string, string>("C", "ऋ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("M", "।"));
            ListSecondPass.Add(new KeyValuePair<string, string>("O", "इ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("P", "ए"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Q", "त्त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("a", "ब"));
            ListSecondPass.Add(new KeyValuePair<string, string>("b", "द"));
            ListSecondPass.Add(new KeyValuePair<string, string>("c", "अ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("d", "म"));
            ListSecondPass.Add(new KeyValuePair<string, string>("e", "भ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("g", "न"));
            ListSecondPass.Add(new KeyValuePair<string, string>("h", "ज"));
            ListSecondPass.Add(new KeyValuePair<string, string>("i", "ष्"));
            ListSecondPass.Add(new KeyValuePair<string, string>("j", "व"));
            ListSecondPass.Add(new KeyValuePair<string, string>("k", "प"));
            ListSecondPass.Add(new KeyValuePair<string, string>("n", "ल"));
            ListSecondPass.Add(new KeyValuePair<string, string>("o", "य"));
            ListSecondPass.Add(new KeyValuePair<string, string>("p", "उ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("q", "त्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("r", "च"));
            ListSecondPass.Add(new KeyValuePair<string, string>("s", "क"));
            ListSecondPass.Add(new KeyValuePair<string, string>("t", "त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("u", "ग"));
            ListSecondPass.Add(new KeyValuePair<string, string>("v", "ख"));
            ListSecondPass.Add(new KeyValuePair<string, string>("w", "ध"));
            ListSecondPass.Add(new KeyValuePair<string, string>("x", "ह"));
            ListSecondPass.Add(new KeyValuePair<string, string>("y", "थ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("z", "श"));
            ListSecondPass.Add(new KeyValuePair<string, string>("!", "ज्ञ"));
            ListSecondPass.Add(new KeyValuePair<string, string>(">", "श्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("?", "रु"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Â", "हृ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("*", "ड़"));
            ListSecondPass.Add(new KeyValuePair<string, string>("(", "ढ़"));
            ListSecondPass.Add(new KeyValuePair<string, string>("#", "घ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("ª", "ङ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("%", "छ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("`", "ञ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("^", "ट"));
            ListSecondPass.Add(new KeyValuePair<string, string>("&", "ठ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("*", "ड"));
            ListSecondPass.Add(new KeyValuePair<string, string>("(", "ढ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("<", "र"));
            ListSecondPass.Add(new KeyValuePair<string, string>(";", "स"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ÿ", ":"));
            ListSecondPass.Add(new KeyValuePair<string, string>("=", "."));
            ListSecondPass.Add(new KeyValuePair<string, string>("º", ")"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ö", "="));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ù", ";"));
            ListSecondPass.Add(new KeyValuePair<string, string>("…", "‘"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ú", "’"));
            ListSecondPass.Add(new KeyValuePair<string, string>("È", "!"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Ü", "%"));
            ListSecondPass.Add(new KeyValuePair<string, string>("æ", "“"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Æ", "”"));
            ListSecondPass.Add(new KeyValuePair<string, string>("®", "+"));
            ListSecondPass.Add(new KeyValuePair<string, string>("œ", "("));
            ListSecondPass.Add(new KeyValuePair<string, string>("Œ", "?"));
            ListSecondPass.Add(new KeyValuePair<string, string>("÷", "/"));
            ListSecondPass.Add(new KeyValuePair<string, string>(")f", "ण"));
            ListSecondPass.Add(new KeyValuePair<string, string>("if", "ष"));
            ListSecondPass.Add(new KeyValuePair<string, string>("If", "क्ष"));
            ListSecondPass.Add(new KeyValuePair<string, string>("em", "झ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("km", "फ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("Tt", "त्त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("w|", "ध्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("cf", "आ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("P]", "ऐ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("b|", "द्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("km", "फ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("pm", "ऊ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("s{", "र्क"));
            ListSecondPass.Add(new KeyValuePair<string, string>("v{", "र्ख"));
            ListSecondPass.Add(new KeyValuePair<string, string>("u{", "र्ग"));
            ListSecondPass.Add(new KeyValuePair<string, string>("#{", "र्घ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("r{", "र्च"));
            ListSecondPass.Add(new KeyValuePair<string, string>("%{", "र्छ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("h{", "र्ज"));
            ListSecondPass.Add(new KeyValuePair<string, string>("^{", "र्ट"));
            ListSecondPass.Add(new KeyValuePair<string, string>("&{", "र्ठ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("*{", "र्ड"));
            ListSecondPass.Add(new KeyValuePair<string, string>("({", "र्ढ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("t{", "र्त"));
            ListSecondPass.Add(new KeyValuePair<string, string>("y{", "र्थ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("b{", "र्द"));
            ListSecondPass.Add(new KeyValuePair<string, string>("w{", "र्ध"));
            ListSecondPass.Add(new KeyValuePair<string, string>("g{", "र्न"));
            ListSecondPass.Add(new KeyValuePair<string, string>("k{", "र्प"));
            ListSecondPass.Add(new KeyValuePair<string, string>("a{", "र्ब"));
            ListSecondPass.Add(new KeyValuePair<string, string>("e{", "र्भ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("d{", "र्म"));
            ListSecondPass.Add(new KeyValuePair<string, string>("n{", "र्ल"));
            ListSecondPass.Add(new KeyValuePair<string, string>("j{", "र्व"));
            ListSecondPass.Add(new KeyValuePair<string, string>("z{", "र्श"));
            ListSecondPass.Add(new KeyValuePair<string, string>(";{", "र्स"));
            ListSecondPass.Add(new KeyValuePair<string, string>("!{", "र्ज्ञ"));
            ListSecondPass.Add(new KeyValuePair<string, string>("k|", "प्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("h|", "ज्र"));
            ListSecondPass.Add(new KeyValuePair<string, string>("o{", "र्य"));


            //THIRD PASS
            ListThirdPass.Add(new KeyValuePair<string, string>("f", "ा"));
            ListThirdPass.Add(new KeyValuePair<string, string>("|", "्र"));
            //rep.Add(new KeyValuePair<string, string>("|", "्"));
            ListThirdPass.Add(new KeyValuePair<string, string>("l", "ि"));
            ListThirdPass.Add(new KeyValuePair<string, string>("{", "ॉ"));
            ListThirdPass.Add(new KeyValuePair<string, string>("\"", "ू"));
            ListThirdPass.Add(new KeyValuePair<string, string>("'", "ु"));
            ListThirdPass.Add(new KeyValuePair<string, string>("_", "ं"));
            ListThirdPass.Add(new KeyValuePair<string, string>("[", "ृ"));
            ListThirdPass.Add(new KeyValuePair<string, string>(@"\", "्"));
            ListThirdPass.Add(new KeyValuePair<string, string>("]", "े"));
            ListThirdPass.Add(new KeyValuePair<string, string>("}", "ै"));
            ListThirdPass.Add(new KeyValuePair<string, string>("Ÿ", "ः"));
            //ListThirdPass.Add(new KeyValuePair<string, string>("f{", "ॉ")); //शर्मा  शमॉ
            ListThirdPass.Add(new KeyValuePair<string, string>("f}", "ौ"));
            ListThirdPass.Add(new KeyValuePair<string, string>("f]", "ो"));
            ListThirdPass.Add(new KeyValuePair<string, string>("F", "ँ"));
            ListThirdPass.Add(new KeyValuePair<string, string>("L", "ी"));
            ListThirdPass.Add(new KeyValuePair<string, string>("l", "ि"));

            //FINAL PASS
            ListFinalPass.Add(new KeyValuePair<string, string>("्ा", "ः"));
            ListFinalPass.Add(new KeyValuePair<string, string>("्ो", "े"));
            ListFinalPass.Add(new KeyValuePair<string, string>("्ौ", "ै"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अो", "ओ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("अा", "आ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("आै", "औ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("आे", "ओ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाो", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाॅ", "ॉ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाे", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंु", "ुं"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ेे", "े"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाे", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंा", "ां"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ाै", "ौ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ैा", "ौ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंृ", "ृं"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ँा", "ाँ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ँू", "ूँ"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ेा", "ो"));
            ListFinalPass.Add(new KeyValuePair<string, string>("ंे", "ें"));


           



        }

        public string ToUnicode(string Shangrila)
        {

            foreach (var item in ListFirstPass)
            {

                if (item.Key.Contains(Shangrila))
                {
                    Shangrila = Shangrila.Replace(item.Key, item.Value);
                }
                else
                {
                    Shangrila = replaceAll(Shangrila, item.Key, item.Value);
                }
            }

            foreach (var item in ListSecondPass)
            {

                if (item.Key.Contains(Shangrila))
                {
                    Shangrila = Shangrila.Replace(item.Key, item.Value);
                }
                else
                {
                    Shangrila = replaceAll(Shangrila, item.Key, item.Value);
                }
            }

            foreach (var item in ListThirdPass)
            {


                if (item.Key.Contains(Shangrila))
                {
                    Shangrila = Shangrila.Replace(item.Key, item.Value);
                }
                else
                {
                    Shangrila = replaceAll(Shangrila, item.Key, item.Value);
                }
            }

            foreach (var item in ListFinalPass)
            {


                if (item.Key.Contains(Shangrila))
                {
                    Shangrila = Shangrila.Replace(item.Key, item.Value);
                }
                else
                {
                    Shangrila = replaceAll(Shangrila, item.Key, item.Value);
                }
            }
            return Shangrila;
        }

        string replaceAll(string str, string find, string replace)
        {
                return str.Replace(find, replace);
        }


       

        public string ShangrilaConfigToUnicode(string ShangrilaFont)
        {
            init();

            ShangrilaFont = ReplaceMixingWords(ShangrilaFont);

            //string stoppedChar = "lLF";
            string stoppedChar = "l";
            string NewShangrila = "";
            var spilitted = ShangrilaFont.Split(' ');

            foreach (var singleWord in spilitted)
            {
                string ProcessWord = singleWord;
                foreach (char stpChar in stoppedChar.ToCharArray())
                {
                    if (singleWord.Contains(stpChar.ToString()))
                    {
                        try
                        {
                            char[] array = singleWord.ToCharArray();
                            char[] temparray = array;


                            for (int index = 0; index < array.Length - 1; index++)
                            {
                                if (array[index] == stpChar)
                                {
                                    char temp = temparray[index]; // Get temporary copy of character
                                    temparray[index] = temparray[index + 1]; // Assign element
                                    temparray[index + 1] = temp; // Assign element
                                    ProcessWord = new string(temparray);
                                    index++;
                                }
                            }

                            //foreach (var item in singleWord.ToCharArray())
                            //{

                            //    if (item == stpChar)
                            //    {
                            //        char temp = temparray[index]; // Get temporary copy of character
                            //        temparray[index] = temparray[index + 1]; // Assign element
                            //        temparray[index + 1] = temp; // Assign element
                            //        ProcessWord = new string(temparray);
                            //        index++;
                            //    }
                            //    index++;

                            //    //int indexof = singleWord.IndexOf(stpChar.ToString());
                            //    //string nextChar = singleWord.Substring(indexof + 1, 1);
                            //    //ProcessWord = singleWord.Replace(stpChar.ToString(), nextChar);
                            //    //char temp = array[indexof]; // Get temporary copy of character
                            //    //array[indexof] = array[indexof + 1]; // Assign element
                            //    //array[indexof + 1] = temp; // Assign element
                            //    //ProcessWord = new string(array); // Return string
                            //}

                            //int indexof = singleWord.IndexOf(stpChar.ToString());
                            //string nextChar = singleWord.Substring(indexof + 1, 1);
                            //ProcessWord = singleWord.Replace(stpChar.ToString(), nextChar);
                            //char temp = array[indexof]; // Get temporary copy of character
                            //array[indexof] = array[indexof + 1]; // Assign element
                            //array[indexof + 1] = temp; // Assign element
                            //ProcessWord = new string(array); // Return string
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
                NewShangrila += ProcessWord + " ";
               
            }

            ShangrilaFont = NewShangrila.Trim();

            //foreach (char stpChar in stoppedChar.ToCharArray())
            //{
            //    if (ShangrilaFont.Contains(stpChar.ToString()))
            //    {
            //        try
            //        {
            //            char[] array = ShangrilaFont.ToCharArray();
            //            int indexof = ShangrilaFont.IndexOf(stpChar.ToString());
            //            string nextChar = ShangrilaFont.Substring(indexof + 1, 1);
            //            ShangrilaFont = ShangrilaFont.Replace(stpChar.ToString(), nextChar);
            //            char temp = array[indexof]; // Get temporary copy of character
            //            array[indexof] = array[indexof + 1]; // Assign element
            //            array[indexof + 1] = temp; // Assign element
            //            ShangrilaFont = new string(array); // Return string
            //        }
            //        catch (Exception)
            //        {
            //        }
                   
            //    }
            //}

            List<string> langSections = new List<string>();
           
            langSections.Add(ShangrilaFont);

            List<string> translatedSections = new List<string>(); ;
            var sectionIsNepali = true;
            for (var i = 0; i < langSections.Count; i++)
            {
                var section = langSections[i];
                if (section == "*n*")
                {
                    sectionIsNepali = true;
                    continue;
                }
                if (section == "*e*")
                {
                    sectionIsNepali = false;
                    continue;
                }
                // this must be a text section
                if (sectionIsNepali)
                {
                    section = ToUnicode(section);
                }
                translatedSections.Add(section);
            }

            string unicode = "";
            foreach (var item in translatedSections)
            {
                unicode += item;
            }

            return unicode ;
        }

        private static string ReplaceMixingWords(string ShangrilaFont)
        {


            //if (ShangrilaFont.Contains("lif"))
            //{
            //    ShangrilaFont = ShangrilaFont.Replace("lif", "षि");
            //}

            //if (ShangrilaFont.Contains("l)f"))
            //{
            //    ShangrilaFont = ShangrilaFont.Replace("l)f", "णि");
            //}

            //if (ShangrilaFont.Contains("lDa"))
            //{
            //    ShangrilaFont = ShangrilaFont.Replace("lDa", "म्बि");
            //}

            //if (ShangrilaFont.Contains("lGh"))
            //{
            //    ShangrilaFont = ShangrilaFont.Replace("lGh", "न्जि");
            //}

            //if (ShangrilaFont.Contains("lGb"))
            //{
            //    ShangrilaFont = ShangrilaFont.Replace("lGb", "न्दि");
            //}
            
         
            return ShangrilaFont;
        }

    }
}
