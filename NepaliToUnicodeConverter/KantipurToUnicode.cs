﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NepaliToUnicodeConverter
{
    class KantipurToUnicode
    {
        List<KeyValuePair<string, string>> rep = new List<KeyValuePair<string, string>>();

        private void init()
        {
            rep.Add(new KeyValuePair<string, string>("ç", "ॐ"));
            rep.Add(new KeyValuePair<string, string>("˜", "ऽ"));
            rep.Add(new KeyValuePair<string, string>("›", "ऽ"));
            rep.Add(new KeyValuePair<string, string>(".", "।"));
            rep.Add(new KeyValuePair<string, string>("'m", "m'"));
            rep.Add(new KeyValuePair<string, string>("]m", "m]"));
            rep.Add(new KeyValuePair<string, string>("Fmf", "mfF"));
            rep.Add(new KeyValuePair<string, string>("Fm", "mF"));
            rep.Add(new KeyValuePair<string, string>("Kfm", "km"));
            rep.Add(new KeyValuePair<string, string>("F'", "'F"));
            rep.Add(new KeyValuePair<string, string>(@"/\|{2,}/g", "|"));
            rep.Add(new KeyValuePair<string, string>(@"/\]{2,}/g", "]"));
            rep.Add(new KeyValuePair<string, string>(@"/\[{2,}/g", "["));
            rep.Add(new KeyValuePair<string, string>(@"/\{{2,}/g", "{"));
            rep.Add(new KeyValuePair<string, string>(@"/\}{2,}/g", "}"));
            rep.Add(new KeyValuePair<string, string>(@"/\}{2,}/g", "}"));
            rep.Add(new KeyValuePair<string, string>(@"/([ejkpq])([\|\[\]\{\}'\\])m/g,", "$1m$2"));
            rep.Add(new KeyValuePair<string, string>("\\", "]\\"));
            rep.Add(new KeyValuePair<string, string>("]", ""));
            rep.Add(new KeyValuePair<string, string>("´", "झ"));
            rep.Add(new KeyValuePair<string, string>(")", "०"));
            rep.Add(new KeyValuePair<string, string>("!", "१"));
            rep.Add(new KeyValuePair<string, string>("@", "२"));
            rep.Add(new KeyValuePair<string, string>("#", "३"));
            rep.Add(new KeyValuePair<string, string>("$", "४"));
            rep.Add(new KeyValuePair<string, string>("%", "५"));
            rep.Add(new KeyValuePair<string, string>("^", "६"));
            rep.Add(new KeyValuePair<string, string>("&", "७"));
            rep.Add(new KeyValuePair<string, string>("*", "८"));
            rep.Add(new KeyValuePair<string, string>("(", "९"));
            rep.Add(new KeyValuePair<string, string>("em", "झ"));
            rep.Add(new KeyValuePair<string, string>("km", "फ"));
            rep.Add(new KeyValuePair<string, string>("Qm", "क्त"));
            rep.Add(new KeyValuePair<string, string>("qm", "क्र"));
            rep.Add(new KeyValuePair<string, string>("jm", "क"));
            rep.Add(new KeyValuePair<string, string>("N", "ल"));
            rep.Add(new KeyValuePair<string, string>("˜", ""));
            rep.Add(new KeyValuePair<string, string>("¡", "ज्ञ्"));
            rep.Add(new KeyValuePair<string, string>("¢", "द्घ"));
            rep.Add(new KeyValuePair<string, string>("1", "ज्ञ"));
            rep.Add(new KeyValuePair<string, string>("2", "द्द"));
            rep.Add(new KeyValuePair<string, string>("4", "द्ध"));
            rep.Add(new KeyValuePair<string, string>(">", "श्र"));
            rep.Add(new KeyValuePair<string, string>("?", "रु"));
            rep.Add(new KeyValuePair<string, string>("B", "द्य"));
            rep.Add(new KeyValuePair<string, string>("I", "क्ष्"));
            rep.Add(new KeyValuePair<string, string>("Q", "त्त"));
            rep.Add(new KeyValuePair<string, string>("ß", "द्म"));
            rep.Add(new KeyValuePair<string, string>("q", "त्र"));
            rep.Add(new KeyValuePair<string, string>("„", "ध्र"));
            rep.Add(new KeyValuePair<string, string>("‹", "ङ्घ"));
            rep.Add(new KeyValuePair<string, string>("•", "ड्ड"));
            rep.Add(new KeyValuePair<string, string>("›", "द्र"));
            rep.Add(new KeyValuePair<string, string>("§", "ट्ट"));
            rep.Add(new KeyValuePair<string, string>("°", "ड्ढ"));
            rep.Add(new KeyValuePair<string, string>("¶", "ठ्ठ"));
            rep.Add(new KeyValuePair<string, string>("¿", "रू"));
            rep.Add(new KeyValuePair<string, string>("Å", "हृ"));
            rep.Add(new KeyValuePair<string, string>("Ë", "ङ्ग"));
            rep.Add(new KeyValuePair<string, string>("Ì", "त्र"));
            rep.Add(new KeyValuePair<string, string>("Í", "ङ्क"));
            rep.Add(new KeyValuePair<string, string>("Î", "ङ्ख"));
            //rep.Add(new KeyValuePair<string, string>("", ""));


            rep.Add(new KeyValuePair<string, string>("Ý", "ट्ठ"));
            rep.Add(new KeyValuePair<string, string>("å", "द्व"));
            rep.Add(new KeyValuePair<string, string>("6«", "ट्र"));
            rep.Add(new KeyValuePair<string, string>("7«", "ठ्र"));
            rep.Add(new KeyValuePair<string, string>("8«", "ड्र"));
            rep.Add(new KeyValuePair<string, string>("9«", "ढ्र"));
            rep.Add(new KeyValuePair<string, string>("«", "्र"));
            rep.Add(new KeyValuePair<string, string>("Ø", "य"));
            rep.Add(new KeyValuePair<string, string>("|", "्र"));
            rep.Add(new KeyValuePair<string, string>("8Þ", "ड़"));
            rep.Add(new KeyValuePair<string, string>("9Þ", "ढ़"));
            rep.Add(new KeyValuePair<string, string>("S", "क्"));
            rep.Add(new KeyValuePair<string, string>("s", "क"));
            rep.Add(new KeyValuePair<string, string>("V", "ख्"));
            rep.Add(new KeyValuePair<string, string>("v", "ख"));
            rep.Add(new KeyValuePair<string, string>("U", "ग्"));
            rep.Add(new KeyValuePair<string, string>("u", "ग"));
            rep.Add(new KeyValuePair<string, string>("£", "घ्"));
            rep.Add(new KeyValuePair<string, string>("3", "घ"));
            rep.Add(new KeyValuePair<string, string>("ª", "ङ"));
            rep.Add(new KeyValuePair<string, string>("R", "च्"));
            rep.Add(new KeyValuePair<string, string>("r", "च"));
            rep.Add(new KeyValuePair<string, string>("5", "छ"));
            rep.Add(new KeyValuePair<string, string>("H", "ज्"));
            rep.Add(new KeyValuePair<string, string>("h", "ज"));
            rep.Add(new KeyValuePair<string, string>("‰", "झ्"));
            rep.Add(new KeyValuePair<string, string>("´", "झ"));
            rep.Add(new KeyValuePair<string, string>("~", "ञ्"));
            rep.Add(new KeyValuePair<string, string>("`", "ञ"));
            rep.Add(new KeyValuePair<string, string>("6", "ट"));
            rep.Add(new KeyValuePair<string, string>("7", "ठ"));
            rep.Add(new KeyValuePair<string, string>("8", "ड"));
            rep.Add(new KeyValuePair<string, string>("9", "ढ"));
            rep.Add(new KeyValuePair<string, string>("0", "ण्"));
            rep.Add(new KeyValuePair<string, string>("T", "त्"));
            rep.Add(new KeyValuePair<string, string>("t", "त"));
            rep.Add(new KeyValuePair<string, string>("Y", "थ्"));
            rep.Add(new KeyValuePair<string, string>("y", "थ"));
            rep.Add(new KeyValuePair<string, string>("b", "द"));
            rep.Add(new KeyValuePair<string, string>("W", "ध्"));
            rep.Add(new KeyValuePair<string, string>("w", "ध"));
            rep.Add(new KeyValuePair<string, string>("G", "न्"));
            rep.Add(new KeyValuePair<string, string>("g", "न"));
            rep.Add(new KeyValuePair<string, string>("K", "प्"));
            rep.Add(new KeyValuePair<string, string>("k", "प"));
            rep.Add(new KeyValuePair<string, string>("ˆ", "फ्"));
            rep.Add(new KeyValuePair<string, string>("A", "ब्"));
            rep.Add(new KeyValuePair<string, string>("a", "ब"));
            rep.Add(new KeyValuePair<string, string>("E", "भ्"));
            rep.Add(new KeyValuePair<string, string>("e", "भ"));
            rep.Add(new KeyValuePair<string, string>("D", "म्"));
            rep.Add(new KeyValuePair<string, string>("d", "म"));
            rep.Add(new KeyValuePair<string, string>("o", "य"));
            rep.Add(new KeyValuePair<string, string>("/", "र"));
            rep.Add(new KeyValuePair<string, string>("µ", "र"));
            //rep.Add(new KeyValuePair<string,string>("¥", 'र्‍'));
            rep.Add(new KeyValuePair<string, string>("N", "ल्"));
            rep.Add(new KeyValuePair<string, string>("n", "ल"));
            rep.Add(new KeyValuePair<string, string>("J", "व्"));
            rep.Add(new KeyValuePair<string, string>("j", "व"));
            rep.Add(new KeyValuePair<string, string>("Z", "श्"));
            rep.Add(new KeyValuePair<string, string>("z", "श"));
            rep.Add(new KeyValuePair<string, string>("i", "ष्"));
            rep.Add(new KeyValuePair<string, string>(":", "स्"));
            rep.Add(new KeyValuePair<string, string>(";", "स"));
            rep.Add(new KeyValuePair<string, string>("X", "ह्"));
            rep.Add(new KeyValuePair<string, string>("x", "ह"));
            rep.Add(new KeyValuePair<string, string>("cf‘", "ऑ"));
            rep.Add(new KeyValuePair<string, string>("c‘f", "ऑ"));
            rep.Add(new KeyValuePair<string, string>("cf}", "औ"));
            rep.Add(new KeyValuePair<string, string>("cf]", "ओ"));
            rep.Add(new KeyValuePair<string, string>("cf", "आ"));
            rep.Add(new KeyValuePair<string, string>("c", "अ"));
            rep.Add(new KeyValuePair<string, string>("O{", "ई"));
            rep.Add(new KeyValuePair<string, string>("O", "इ"));
            rep.Add(new KeyValuePair<string, string>("pm", "ऊ"));
            rep.Add(new KeyValuePair<string, string>("p", "उ"));
            rep.Add(new KeyValuePair<string, string>("C", "ऋ"));
            rep.Add(new KeyValuePair<string, string>("P]", "ऐ"));
            rep.Add(new KeyValuePair<string, string>("P", "ए"));
            rep.Add(new KeyValuePair<string, string>("f‘", "ॉ"));
            rep.Add(new KeyValuePair<string, string>("\"", "ू"));
            rep.Add(new KeyValuePair<string, string>("'", "ु"));
            rep.Add(new KeyValuePair<string, string>("+", "ं"));
            rep.Add(new KeyValuePair<string, string>("f", "ा"));
            rep.Add(new KeyValuePair<string, string>("[", "ृ"));
            rep.Add(new KeyValuePair<string, string>("\\", "्"));
            rep.Add(new KeyValuePair<string, string>("]", "े"));
            rep.Add(new KeyValuePair<string, string>("}", "ै"));
            rep.Add(new KeyValuePair<string, string>("F", "ा"));
            rep.Add(new KeyValuePair<string, string>("//F", "ँ"));
            rep.Add(new KeyValuePair<string, string>("“", "ँ"));
            rep.Add(new KeyValuePair<string, string>("L", "ी"));
            rep.Add(new KeyValuePair<string, string>("//M", "ः"));
            rep.Add(new KeyValuePair<string, string>("M", ":"));
            rep.Add(new KeyValuePair<string, string>("¨", "ङ्ग"));
            rep.Add(new KeyValuePair<string, string>("®", "र"));
            rep.Add(new KeyValuePair<string, string>("©", "र"));
            rep.Add(new KeyValuePair<string, string>("Â", "र"));
            rep.Add(new KeyValuePair<string, string>("È", "ष"));
            rep.Add(new KeyValuePair<string, string>("Ô", "क्ष"));
            rep.Add(new KeyValuePair<string, string>("ø", "य्"));
            rep.Add(new KeyValuePair<string, string>("ı", "द्र"));
            rep.Add(new KeyValuePair<string, string>("Œ", "त्त्"));
            rep.Add(new KeyValuePair<string, string>("œ", "त्र्"));
            rep.Add(new KeyValuePair<string, string>("˘", "श्र्"));
            rep.Add(new KeyValuePair<string, string>("˚", "फ"));
            rep.Add(new KeyValuePair<string, string>("≈", "ह्"));
            rep.Add(new KeyValuePair<string, string>("⋲", "ह्"));
            rep.Add(new KeyValuePair<string, string>("◊", "ङ्ख"));
            rep.Add(new KeyValuePair<string, string>("", "फ्"));
            rep.Add(new KeyValuePair<string, string>("", "ट्ठ"));
            rep.Add(new KeyValuePair<string, string>("्ा", ""));
            rep.Add(new KeyValuePair<string, string>("्ो", "े"));
            rep.Add(new KeyValuePair<string, string>("्ौ", "ै"));
            rep.Add(new KeyValuePair<string, string>("अो", "ओ"));
            rep.Add(new KeyValuePair<string, string>("अा", "आ"));
            rep.Add(new KeyValuePair<string, string>("आै", "औ"));
            rep.Add(new KeyValuePair<string, string>("आे", "ओ"));
            rep.Add(new KeyValuePair<string, string>("ाो", "ो"));
            rep.Add(new KeyValuePair<string, string>("ाॅ", "ॉ"));
            rep.Add(new KeyValuePair<string, string>("ाे", "ो"));
            rep.Add(new KeyValuePair<string, string>("ंु", "ुं"));
            rep.Add(new KeyValuePair<string, string>("ेे", "े"));
            rep.Add(new KeyValuePair<string, string>("अै", "अ‍ै"));
            rep.Add(new KeyValuePair<string, string>("ाे", "ो"));
            rep.Add(new KeyValuePair<string, string>("अे", "अ‍े"));
            rep.Add(new KeyValuePair<string, string>("ंा", "ां"));
            rep.Add(new KeyValuePair<string, string>("अॅ", "अ‍ॅ"));
            rep.Add(new KeyValuePair<string, string>("ाै", "ौ"));
            rep.Add(new KeyValuePair<string, string>("ैा", "ौ"));
            rep.Add(new KeyValuePair<string, string>("ंृ", "ृं"));
            rep.Add(new KeyValuePair<string, string>("ँा", "ाँ"));
            rep.Add(new KeyValuePair<string, string>("ँू", "ूँ"));
            rep.Add(new KeyValuePair<string, string>("ेा", "ो"));
            rep.Add(new KeyValuePair<string, string>("ंे", "ें"));
            rep.Add(new KeyValuePair<string, string>("े्", "्े"));
            rep.Add(new KeyValuePair<string, string>("/l((.्)?.)/g", "$1ि"));
            rep.Add(new KeyValuePair<string, string>("/ि्(.)/g", "्$1ि"));
            rep.Add(new KeyValuePair<string, string>("/िं्(.)/g", "्$1िं"));
            rep.Add(new KeyValuePair<string, string>("/(.[ािीुूृेैोौं:ँॅ]*){/g", "र्$1"));
            rep.Add(new KeyValuePair<string, string>("{", "र्"));
            rep.Add(new KeyValuePair<string, string>("l", "ि"));
            rep.Add(new KeyValuePair<string, string>("=", "."));
            rep.Add(new KeyValuePair<string, string>("_", ")"));
            rep.Add(new KeyValuePair<string, string>("Ö", "="));
            rep.Add(new KeyValuePair<string, string>("Ù", ";"));
            rep.Add(new KeyValuePair<string, string>("…", "‘"));
            rep.Add(new KeyValuePair<string, string>("Ú", "’"));
            rep.Add(new KeyValuePair<string, string>("Û", "!"));
            rep.Add(new KeyValuePair<string, string>("Ü", "%"));
            rep.Add(new KeyValuePair<string, string>("æ", "“"));
            rep.Add(new KeyValuePair<string, string>("Æ", "”"));
            rep.Add(new KeyValuePair<string, string>("±", "+"));
            rep.Add(new KeyValuePair<string, string>("-", "("));
            rep.Add(new KeyValuePair<string, string>("<", "?"));
            rep.Add(new KeyValuePair<string, string>("¬", "…"));
            rep.Add(new KeyValuePair<string, string>("Ò", "…"));
            rep.Add(new KeyValuePair<string, string>("÷", "/"));
            rep.Add(new KeyValuePair<string, string>("‐", "("));
            rep.Add(new KeyValuePair<string, string>("⁄", "!"));
            rep.Add(new KeyValuePair<string, string>("∞", "%"));
            rep.Add(new KeyValuePair<string, string>("≠", "="));
            rep.Add(new KeyValuePair<string, string>("≤", ";"));
            rep.Add(new KeyValuePair<string, string>("≥", "."));
        }

        public string kantipurToUnicode(string kantipur)
        {
            foreach (var item in rep)
            {
                if (item.Key.Contains(kantipur))
                {
                    //return item.Value;
                    kantipur = kantipur.Replace(item.Key, item.Value);

                }
                else
                {
                    kantipur = replaceAll(kantipur, item.Key, item.Value);
                }
            }

            return kantipur;
        }

        string replaceAll(string str, string find, string replace)
        {
            return str.Replace(find, replace);
        }

        public string kantipurConfigToUnicode(string kantipurFont)
        {
            init();
            List<string> langSections = new List<string>();
            var found = kantipurFont.IndexOf("l");
            while (found >= 0)
            {

                langSections.Add(kantipurFont.Substring(0, found));
                langSections.Add(kantipurFont.Substring(found, found + 3));

                kantipurFont = kantipurFont.Substring(found + 3);
                found = kantipurFont.IndexOf("l");
            }

            langSections.Add(kantipurFont);

            List<string> translatedSections = new List<string>(); ;
            var sectionIsNepali = true;
            for (var i = 0; i < langSections.Count; i++)
            {
                var section = langSections[i];
                if (section == "*n*")
                {
                    sectionIsNepali = true;
                    continue;
                }
                if (section == "*e*")
                {
                    sectionIsNepali = false;
                    continue;
                }
                // this must be a text section
                if (sectionIsNepali)
                {
                    section = kantipurToUnicode(section);
                }
                translatedSections.Add(section);
            }

            string unicode = "";
            foreach (var item in translatedSections)
            {
                unicode += item;
            }
            return unicode ;
        }

    }
}
